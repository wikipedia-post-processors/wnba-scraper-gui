/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

// Wait for the deviceready event before using any of Cordova's device APIs.
// See https://cordova.apache.org/docs/en/latest/cordova/events/events.html#deviceready
document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {
    // Cordova is now initialized. Have fun!

    console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);
    document.getElementById('deviceready').classList.add('ready');
}


function resetInputErrorMsg() {
  /* if an invalid input message is displayed, it is removed. Otherwise, does nothing */
  const messageElement = document.getElementById('input-error-message');
  messageElement.textContent = '';
}


function submitQuery() {
  /* handles form submit */
  // if an invalid input message is displayed, remove it
  resetInputErrorMsg();

  // get form data
  const firstName = document.getElementById('first-name').value;
  const lastName = document.getElementById('last-name').value;
  const year = document.getElementById('year').value;

  console.log('testing submitQuery');

  const baseUrl = 'http://localhost:3000/player';  // TODO: update for production or query window.location
  let reqUrl = baseUrl + '?firstname=' + firstName + '&lastname=' + lastName;
  if (year) {
    reqUrl += '&year=' + year;
  }

  // make request and define callback
  makeGET(reqUrl, true, (resData) => {
    displayResult(resData);
  });
}


function displayResult(res) {
  console.log('in displayResult, creating table for ' + res);

  // display stats data
  const yearData = res.yearData;

  // add a new empty table to the output section
  const table = document.createElement('table');
  document.getElementById('output').appendChild(table);

  // add thead with empty header row
  const thead = document.createElement('thead');
  table.appendChild(thead);
  const theadRow = document.createElement('tr');
  thead.appendChild(theadRow);

  // add tbody with empty data row
  const tbody = document.createElement('tbody');
  table.appendChild(tbody);
  const tbodyRow = document.createElement('tr');
  tbody.appendChild(tbodyRow);

  // add a new dt and th to the table for each stat
  for (const key of Object.keys(yearData)) {
    const value = yearData[key];
    // add a new header to the header row
    const th = document.createElement('th');
    theadRow.appendChild(th);
    th.textContent = key;

    // add a new td to the body row
    const td = document.createElement('td');
    tbodyRow.appendChild(td);
    td.textContent = value;
  }

  // display awards data TODO: use awards instead of achievements
  const achievements = res.achievements;
  const ul = document.createElement('ul');
  document.getElementById('output').appendChild(ul);

  for (const award of achievements) {
    const li = document.createElement('li');
    ul.appendChild(li);
    li.textContent = award;
  }

  console.log('\ndone');
}


function makeGET(url, isAsync, callback) {
  /* makes a GET request to the server and executes callback, which is provided with the response text
  callback: function to execute after successful POST and response from server. Receives parsed response object
  */
  console.log('making GET to ', url);

  let req = new XMLHttpRequest();
  req.open('GET', url);
  req.setRequestHeader('content-type', 'application/json');

  // define callback for response
  if (isAsync) {
    let response;
    req.addEventListener('load', () => {
      if(req.status >= 200 && req.status < 400){
        // if a stats page wasn't available for the given name
        console.log('parsing this response text:\n' + req.responseText);
  
        if (req.responseText === 'NO_PLAYER_FOUND') {
          console.log('server said no. Try a different name');
          document.getElementById('input-error-message').textContent = 'No player found. Try a different name';
          return;
        } else if (req.responseText === 'YEAR_NOT_FOUND') {
          console.log('server said no. Try a different year');
          document.getElementById('input-error-message').textContent = 'No data found for that year. Try a different year';
          return;
        }

        response = JSON.parse(req.responseText);

        // generic robustness
        if (!response) {
          console.log('received a response, but it was undefined');
          return;
        }
  
        console.log('received response:\n', response);
        callback(response);
      } else {
        // server responded with error code
        console.log("Error in network request: " + req.statusText);
    }});
  }

  // send the request
  req.send();
}

// form submit handler
document.getElementById('player-search').addEventListener('submit', (event) => {
  event.preventDefault();  // don't want a refresh and then a re-render
  submitQuery();
});

