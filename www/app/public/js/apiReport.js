/* this is ready in views/js/utils.js. Needs Babel
import makeGET from '/views/js/utils.js';
*/

/* this can be imported after using Babel */
function makeGET(url, isAsync, callback) {
    /* makes a GET request to the server and executes callback, which is provided with the response text
    callback: function to execute after successful POST and response from server. Receives parsed response object
    */
    console.log('making GET to ', url);
  
    let req = new XMLHttpRequest();
    req.open('GET', url);
    req.setRequestHeader('content-type', 'application/json');
  
    // define callback for response
    if (isAsync) {
      let response;
      req.addEventListener('load', () => {
        if(req.status >= 200 && req.status < 400){
          // if a stats page wasn't available for the given name
          console.log('parsing this response text:\n' + req.responseText);
    
          if (req.responseText === 'NO_PLAYER_FOUND') {
            console.log('server said no. Try a different name');
            document.getElementById('input-error-message').textContent = 'No player found. Try a different name';
            return;
          } else if (req.responseText === 'YEAR_NOT_FOUND') {
            console.log('server said no. Try a different year');
            document.getElementById('input-error-message').textContent = 'No data found for that year. Try a different year';
            return;
          }
  
          response = JSON.parse(req.responseText);
  
          // generic robustness
          if (!response) {
            console.log('received a response, but it was undefined');
            return;
          }
    
          console.log('received response:\n', response);
          callback(response);
        } else {
          // server responded with error code
          console.log("Error in network request: " + req.statusText);
      }});
    }
  
    // send the request
    req.send();
  }
  

  function getApiUsageData() {
    /* makes AJAX GET request to server and returns parsed results */

    // make request and define callback
    const reqUrl = 'http://localhost:3000/api/api-usage'
    makeGET(reqUrl, true, (resData) => {
        displayResult(resData);
    });
}


function displayResult(resData) {
    /* displays result by filling in the two uls */

    // define text that goes in each li
    const wnbaExplanationText = ' requests received from service ';
    const nbaExplanationText = ' requests received by sister NBA API from service ';

    // make li tags for wnba data and add them to the wnba ul
    const wnbaUl = document.getElementById('wnba-req-report');
    let count = 1;
    for (const numberOfReqs of resData.wnba) {
        // make the li
        const li = document.createElement('li');
        wnbaUl.appendChild(li);
        li.textContent = numberOfReqs + wnbaExplanationText + count;

        ++count;
    }

    // make li tags for nba data and add them to the nba ul
    const nbaUl = document.getElementById('nba-req-report');
    count = 1;
    for (const numberOfReqs of resData.nba) {
        // make the li
        const li = document.createElement('li');
        nbaUl.appendChild(li);
        li.textContent = numberOfReqs + nbaExplanationText + count;

        ++count;
    }
}


// get data just once, on page load
window.addEventListener('DOMContentLoaded', () => {
    getApiUsageData();
});
