/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

// Wait for the deviceready event before using any of Cordova's device APIs.
// See https://cordova.apache.org/docs/en/latest/cordova/events/events.html#deviceready
document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {
    // Cordova is now initialized. Have fun!

    console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);
    document.getElementById('deviceready').classList.add('ready');
}


function simulateGET() {
  /* load sample json data--simulates server res */
  /*
  from https://developer.mozilla.org/en-US/docs/Web/API/File_System_Access_API#accessing_files

  const pickerOpts = {
    types: [
      {
        description: 'Images',
        accept: {
          'image/*': ['.png', '.gif', '.jpeg', '.jpg']  // change this to .json
        }
      },
    ],
    excludeAcceptAllOption: true,
    multiple: false
  };

  async function getTheFile() {
    // open file picker
    [fileHandle] = await window.showOpenFilePicker(pickerOpts);

    // get file contents
    const fileData = await fileHandle.getFile();

    // then use https://developer.mozilla.org/en-US/docs/Web/API/FileReader
  }
  */

  return;
}


// global to avoid re-compiling templates
var hbTemplates = {}


function prepareHandlebars() {
  // register Handlebars partials
  let source = document.getElementById("result-handlebars").innerHTML;
  Handlebars.registerPartial('result', source);
  
  // source = document.getElementById("nav-handlebars").innerHTML;
  // Handlebars.registerPartial('nav', source);
  
  // compile Handlebars templates and save references to them
  source = document.getElementById("home-handlebars").innerHTML;
  hbTemplates.home = Handlebars.compile(source);  // this will be returned to be used first

  source = document.getElementById("home-handlebars").innerHTML;
  hbTemplates.apiReport = Handlebars.compile(source);
}


function useTemplate(template, context) {
  // plugs a handlebars template into the page, replacing any that was already there
  document.getElementById('root').innerHTML = template(context);
}


function initializeHome() {
  // prepares handlebars templates and uses the home template in the current page
  console.log('TESTING BOI');
  
  prepareHandlebars();

  // use home.handlebars view in index.html
  useTemplate(hbTemplates.home, {years: [2020, 2019, 2018]});

  // add onclicks to simulate nav
  document.getElementById('nav-wnba-stats').addEventListener('click', () => {
    // use home.handlebars view in index.html
    useTemplate(hbTemplates.home, {years: [2020, 2019, 2018]});
  });

  document.getElementById('nav-api-report').addEventListener('click', () => {
    // use apiReport.handlebars view in index.html
    useTemplate(hbTemplates.apiReport, {years: [2020, 2019, 2018]});
  });
}


window.addEventListener('DOMContentLoaded', () => {
  initializeHome();
});
